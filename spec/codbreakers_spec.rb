require "../lib/main.rb"
describe Cb do 
    context 'When testing the #generator method' do 
      
      it "returns an array size of 4" do 
         subject = Cb.new 
         ary_size = subject.generator.size 
         expect(ary_size).to eq 4
      end

        it "returns array of integers" do
          subject = Cb.new
          aray = subject.generator
          integers_array = aray.select {|x| x.is_a? Integer}
          expect(aray).to eq integers_array
        end

    end

    context "When testing the #compare method"
    
      it "returns all '+' when codes match" do
        subject = Cb.new
        expect(subject.compare([1,2,3,4], [1,2,3,4])).to eq ["+", "+", "+", "+"]
      end

      it "returns all ' ' when codes does not match" do
        subject = Cb.new
        expect(subject.compare([1,2,3,4], [5,5,5,5])).to eq [" ", " ", " ", " "]
      end

      it "returns all '-' when codes numbers are not in place" do
        subject = Cb.new
        expect(subject.compare([1,2,3,4], [4,3,2,1])).to eq [" ", " ", " ", " "]
      end

      it "will check rest of numbers if match was found" do
        subject = Cb.new
        expect(subject.compare([1,2,3,4], [4,3,2,1])).to eq [" ", " ", " ", " "]
      end
end